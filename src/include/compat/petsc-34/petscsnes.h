typedef SNESNormType SNESNormSchedule;
#define SNES_NORM_ALWAYS    SNES_NORM_FUNCTION
#define SNESSetNormSchedule SNESSetNormType
#define SNESGetNormSchedule SNESGetNormType
#define SNESConvergedSkip   SNESSkipConverged
